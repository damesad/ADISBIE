Aplicativo de Gestion de Biblioteca

#Programa editor de Codigo : http://www.sublimetext.com/

#Servidor Bases de Datos : MYSQL

#Programa Administrador Base de Datos : phpmyadmin

#Servidor WEB : APACHE

#leguaje del lado del servidor : PHP

#FrameworK del lado del servidor : http://www.codeigniter.com/

#Libreria usadas en el framework : http://www.grocerycrud.com/

#Tutorial Uso de la libreria : http://sourcezilla.com/articulos/programacion/como-realizar-un-crud-sencillo-en-php-y-codeigniter-desde-cero.html

_____________________________

#Instarla Servidor apache y mysql.
    ~$ parts install apache2 mysql php5
#Arracar Servidores
    ~$ parts start apache2 mysql
  
#Instalar Composer.
    ~$ parts install composer
#Actualizar composer
    ~$ composer self-update
#Instalar Codeigniter - clonar de repositorio github
    ~$ git clone https://github.com/bcit-ci/CodeIgniter.git
     
#Renombrar directorio proyecto, de CodeIgniter por adisbie

#Cambiar ruta del servidor
    ~$ nano /home/codio/.parts/etc/apache2/httpd.conf 
#buscar linea  document root (workspace) y cambiar por
    -- document root (workspace/adisbie)
#para guardar cambios  (ctrl+x) luego (enter)

#Cambiar Contraseña por defecto
    ~$ mysql -u root mysql
    ~$ mysql>update user set password=PASSWORD('tucontrasenia') where user='root';

#Instalar gestor de base de datos PHPMYADMIN
    ~$ parts install phpmyadmin

#Instalar Grocerycrud - clonar de repositorio github
    ~$ git clone https://github.com/scoumbourdis/grocery-crud.git

#mover fichero assets a adisbie/
    ~$ rv grocery-crud/assets adisbie
#codio tambine permite (arrastrar-soltar)



