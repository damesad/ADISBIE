<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Heredamos de la clase CI_Controller */
class SingIn extends CI_Controller {

    public $persona;
    
   function __construct()
  {
        parent::__construct();

    // Se carga el modelo de usuarios.
		$this->load->model('Users');
 
		// Se carga el helper url y form.
		$this->load->helper('url');
		
		// Se le asigna a la informacion a la variable $user.
		$this->persona = new Users();
  }
 

  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    redirect('Welcome');
  }
    
  function admin(){
     
      $documento = $this->input->get_post('documento');
      $pass = $this->input->get_post('pass');
     
      if(  $pass!=''){
          $crud=null;
          redirect('Tablero/index');
      }
      else{
          $this->load->view('sing_in/administrador_tpl');
      }
      
  }
    
    function bibliotecario(){
     
      $documento = $this->input->get_post('documento');
      $pass = $this->input->get_post('pass');
      
      
      if( $pass!=''){
          $crud=null;
          redirect('Tablero/biblio');
      }
      else{
          $this->load->view('sing_in/bibliotecario_tpl');
      }
      
  }
    
   function user(){

       
      $documento = $this->input->get_post('documento');
      $pass = $this->input->get_post('pass');
      
      
      if( $pass!=''){
          $crud=null;
          redirect('Tablero/user');
      }
      else{
          $this->load->view('sing_in/bibliotecario_tpl');
      }
	
       }


  
}