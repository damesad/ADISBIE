<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Heredamos de la clase CI_Controller */
class Tablero extends CI_Controller {
    function __construct()
  {
    
    parent::__construct();
    /* Añadimos el helper al controlador */
    $this->load->helper('url');
  }
 
  function index()
  {
      $this->load->view('tablero/administrador_tpl');
  }
    
    
    function biblio()
  {
        //inicio sesio bibliotecario
      $this->load->view('tablero/bibliotecario_tpl');
  }
  
       function user()
  {
        //inicio sesio bibliotecario
      $this->load->view('tablero/usuario_tpl');
  }
 
    
  //Acciones de Administrador
  //
  function Usuarios(){
     redirect('Usuarios');
  }
  
 //Aciones de Usuario
    
  function Prestamos(){
   redirect('Prestamos');
  } 
  
  function Reservar(){
     redirect('Reservaciones');
  }
    
    //acciones de bibliotecario
    
  function Autores(){
    redirect('Autores');
  } 
    
  function Materiales(){
      redirect('Materiales');
  } 
    
  function Editoriales(){
       redirect('Editoriales');
  }  
  
  function AutorMateriales(){
      redirect('AutorMateriales');
  }
    
  function Devoluciones(){
      redirect('Devoluciones');
  }
    
  function sancionar(){
       redirect('Sanciones');
  }
    
} 