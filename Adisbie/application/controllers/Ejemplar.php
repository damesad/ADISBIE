<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ejemplar extends CI_Controller {
  function __construct()
  {
    ini_set('date.timezone', 'America/Bogota');
    parent::__construct();
    $this->load->database();
    $this->load->library('Grocery_CRUD');
    $this->load->helper('url');
  }
	public function index()
	{
         try{
    $crud = new grocery_CRUD();
    $crud->set_theme('flexigrid');

    /* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('ejemplar');
    $crud->set_subject('Editoriales');
    $crud->set_language('spanish');
  
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'nombre',
      'descripcion'
    );

    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'cod_ejemplar',
      'nombre',
      'descripcion'
    );
    
    $output = $crud->render();
    $this->load->view('crud/ejemplar_tpl', $output);

    }catch(Exception $e){
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
	}
    
}
