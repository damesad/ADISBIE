<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reservaciones extends CI_Controller {
  function __construct()
  {
    ini_set('date.timezone', 'America/Bogota');
    parent::__construct();
    $this->load->database();
    $this->load->library('Grocery_CRUD');
    $this->load->helper('url');
  }
	public function index()
	{
        try{
    $crud = new grocery_CRUD();
    $crud->set_theme('twitter-bootstrap');

    /* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('reserva');
    $crud->set_subject('Reservaciones');
    $crud->set_language('spanish');

    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'numero_documento',
      'fecha_reserva',
      'fecha_limite_reserva'
     
    );

    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'cod_material',
      'numero_documento',
      'fecha_reserva',
      'fecha_limite_reserva'
    );

    $output = $crud->render();
    $this->load->view('crud/reservacion_tpl', $output);

    }catch(Exception $e){
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
	}
    
}
