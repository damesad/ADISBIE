<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prestamos extends CI_Controller {
     function __construct()
  {
    ini_set('date.timezone', 'America/Bogota');
    parent::__construct();

    /* Cargamos la base de datos */
    $this->load->database();

    /* Cargamos la libreria*/
    $this->load->library('Grocery_CRUD');

    /* Añadimos el helper al controlador */
    $this->load->helper('url');
  }
  
  function index()
  {
    try{

    /* Creamos el objeto */
    $crud = new grocery_CRUD();

    /* Seleccionamos el tema */
    $crud->set_theme('twitter-bootstrap');

    /* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('prestamo');

    /* Le asignamos un nombre */
    $crud->set_subject('Prestamo');

    /* Asignamos el idioma español */
    $crud->set_language('spanish');

    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'numero_documento',
      'cod_material',
      'cod_tipo_prestamo',
      'fecha_de_prestamo',
      'fecha_limite'
    );

    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'cod_prestamo', 
      'numero_documento', 
      'cod_material',
      'cod_tipo_prestamo',
      'fecha_de_prestamo',
      'fecha_limite'
    );
   
 
    $crud->set_relation('numero_documento','persona','numero_documento');
        
    $crud->set_relation('cod_material','material','cod_material');
    
    $crud->set_relation('cod_tipo_prestamo','tipo_prestamo','nombre');
    /* Generamos la tabla */
    $output = $crud->render();

    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
    $this->load->view('crud/prestamo_tpl', $output);

    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  }
}