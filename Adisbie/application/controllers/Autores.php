<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Autores extends CI_Controller {
        function __construct()
  {
    ini_set('date.timezone', 'America/Bogota');
    parent::__construct();

    /* Cargamos la base de datos */
    $this->load->database();

    /* Cargamos la libreria*/
    $this->load->library('Grocery_CRUD');

    /* Añadimos el helper al controlador */
    $this->load->helper('url');
  }

	public function index()
	{
    
		try{
    $crud = new grocery_CRUD();
    $crud->set_theme('twitter-bootstrap');

    /* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('autor');
    $crud->set_subject('Autores');
    

    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'cod_autor',
      'nombre',
      'fecha_nacimiento'
    );

    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'cod_autor',
      'nombre',
      'fecha_nacimiento'
    );

    $output = $crud->render();
    $this->load->view('crud/autores_tpl', $output);

    }catch(Exception $e){
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
	}
    
}
