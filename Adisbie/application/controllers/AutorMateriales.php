<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AutorMateriales extends CI_Controller {
  function __construct()
  {
    ini_set('date.timezone', 'America/Bogota');
    parent::__construct();
    $this->load->database();
    $this->load->library('Grocery_CRUD');
    $this->load->helper('url');
  }
	public function index()
	{
        try{
    $crud = new grocery_CRUD();
    $crud->set_theme('twitter-bootstrap');

    /* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('autor_material');
    $crud->set_subject('AutorMateriales');
    $crud->set_language('spanish');

    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'cod_autor',
      'cod_material'
      
    );

    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'cod', 
      'cod_autor',
      'cod_material'
    );
    $crud->display_as('cod_autor','nombre'); 
    $crud->display_as('cod_material','material');
    $crud->set_subject('autor');$crud->set_relation('cod_autor','autor','nombre');
    $crud->set_subject('material');$crud->set_relation('cod_material','material','nombre');
    $output = $crud->render();
    $this->load->view('crud/AutorMaterial_tpl', $output);

    }catch(Exception $e){
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
	}
    
}
