<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Materiales extends CI_Controller {
  function __construct()
  {
    ini_set('date.timezone', 'America/Bogota');
    parent::__construct();
    $this->load->database();
    $this->load->library('Grocery_CRUD');
    $this->load->helper('url');
  }
	public function index()
	{
         try{
    $crud = new grocery_CRUD();
    $crud->set_theme('twitter-bootstrap');

    /* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('material');
    $crud->set_subject('Materiales');
    $crud->set_language('spanish');

    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'cod_material',
      'nombre',
      'cod_ejemplar',
      'cod_editorial',
      'cod_tipo_material'
    );

    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'nombre',
      'cod_ejemplar',
      'cod_editorial',
      'cod_tipo_material'
    );
             
    $crud->display_as('cod_editorial','Nombre Editorial');
    $crud->set_relation('cod_editorial','editorial','nombre');
     
    $crud->display_as('cod_tipo_material','Tipo Material');
    $crud->set_relation('cod_tipo_material','tipo_material','Descripcion');    
             
    $output = $crud->render();
    $this->load->view('crud/material_tpl', $output);

    }catch(Exception $e){
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
	}
    
}
