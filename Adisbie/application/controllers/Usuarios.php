<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {
     function __construct()
  {
    ini_set('date.timezone', 'America/Bogota');
    parent::__construct();

    /* Cargamos la base de datos */
    $this->load->database();

    /* Cargamos la libreria*/
    $this->load->library('Grocery_CRUD');

    /* Añadimos el helper al controlador */
    $this->load->helper('url');
  }

	public function index()
	{
        try{

    /* Creamos el objeto */
    $crud = new grocery_CRUD();

    /* Seleccionamos el tema */
    $crud->set_theme('twitter-bootstrap');

    /* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('persona');

    /* Le asignamos un nombre */
    $crud->set_subject('Usuarios');

    /* Asignamos el idioma español */
    $crud->set_language('spanish');

    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
     'numero_documento',
      'tipo_documento',
      'nombre1',
      'apellido1',
      'direccion',
      'telefono',
      'email',
      'fecha_nacimiento',
      'codigo_tipo_persona'
    );

    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'numero_documento',
      'nombre1',
      'apellido1',
      'codigo_tipo_persona'
    );

    /* Generamos la tabla */
    $output = $crud->render();

    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
    $this->load->view('crud/user_tpl', $output);

    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
	}
    
}
