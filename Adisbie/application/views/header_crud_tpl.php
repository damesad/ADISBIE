<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Gestion de Bibliotecas</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

    <style>
        article, aside, details, figcaption, figure, footer, header, hgroup, main, menu, nav, section, summary {
  display: block; }

 html {
  line-height: 1.5;
  font-family: "Roboto",sans-serif;
  font-weight: normal;
  color: rgba(0,0,0,0.87);
}
        
body{
    margin:0
}
        
.z-depth-1, nav, .card-panel, .card, .toast, .btn, .btn-large, .btn-floating, .dropdown-content, .collapsible, ul.side-nav {
  -webkit-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
  -moz-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
  box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12); }

 nav {
  color: #fff;
  background-color: #ee6e73;
  width: 100%;
  height: 56px;
 line-height: 64px;
  
  overflow: hidden; }
  nav a {
    color: #fff; }
  nav .nav-wrapper {
    position: relative;
    height: 100%; }
    nav .nav-wrapper i {
      font-size: 2rem; }
  @media only screen and (min-width : 993px) {
    nav a.button-collapse {
      display: none; }
 }

nav ul a {
  font-size: 1rem;
  color: #fff;
  display: inline-block;
  padding: 0px 15px;
  list-style-type: none;
}

nav ul li {
   list-style-type: none;
  -webkit-transition: background-color 0.3s;
  -moz-transition: background-color 0.3s;
  -o-transition: background-color 0.3s;
  -ms-transition: background-color 0.3s;
  transition: background-color 0.3s;
  float: left;
  padding: 0px;
  display: list-item;
  text-align: -webkit-match-parent;
}

nav .brand-logo {
    position: absolute;
    color: #fff;
    display: inline-block;
    font-size: 2.1rem;
    padding: 0; }

.right {
  float: right !important; }

.container {
  padding: 0 1.5rem;
  margin: 0 auto;
  max-width: 1280px;
  width: 90%; }

.card-panel {
  padding: 20px;
  margin: 0.5rem 0 1rem 0;
  -webkit-border-radius: 2px;
  -moz-border-radius: 2px;
  border-radius: 2px;
  background-clip: padding-box;
  background-color: #fff; }

a {
  text-decoration: none;
}

ul li {
  list-style-type: none;
}

    </style>
</head>
 
<body>
<header>
  <nav>
    <div class="nav-wrapper">
      <a href="/" class="brand-logo" style="margin-left:2rem">Sistema Gestion Bibliotecas <span style="font-size:16px"> - Regresar</span></a>
        
      <ul id="nav-mobile" class="right hide-on-med-and-down">
       
        
      </ul>
    </div>
  </nav>
</header>
<section class="container">

