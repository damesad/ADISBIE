<?php
$this->load->view('header_singin_tpl');
?>

<div class="card-panel yellow">
	
    <form class="container card-panel row" style="margin:0 auto" method="POST">
        <h4>Iniciar Sesion</h4>
        <div class="input-field col s8">
        <input id="documento" name="documento" type="text" class="validate">
        <label for="documento">Numero de Documento</label>
        </div>
        <div class="input-field col s8 ">
        <input id="pass" name="pass" type="password" class="validate">
        <label for="pass">Contraseña</label>
        </div>
        <input class="btn" type="submit" value="Ingresar">
    </form>
    
</div>

  
<?php
$this->load->view('footer_tpl');
?>