<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('header_crud_tpl');
?>
<style>
    select{display:inline-block;}
    
</style>
<?php
foreach($css_files as $file): ?>
<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<h1>Adminstar Tus Reservaciones</h1>
<div>
<?php echo $output; ?>
</div>

<?php
$this->load->view('footer_tpl');
?>
