<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Gestion de Bibliotecas</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.95.3/css/materialize.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
</head>
 
<body>
<header>
  <nav>
    <div class="nav-wrapper">
      <a href="/" class="brand-logo" style="margin-left:2rem">Sistema Gestion Bibliotecas</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <a href="/index.php"><li class="large mdi-action-home" style='font-size:2rem;'></li>Volver al Inicio</a> 
      </ul>
    </div>
  </nav>
</header>
<section class="container">