<?php
$this->load->view('header_user_tpl');
?>

<div class="card-panel yellow">
	
    <div class="row" style="margin:0 auto;width:90%;">
       <div>
          <h4 class="s10">Seleccione Para Administrar</h4>
       </div>
       <div class="divider black "></div>
       <div class="section">
            <a href="Usuarios" class="btn s3">Nuevo Usuario </a>
       </div>
       <div class="divider red "></div>
       <div class="section">
        <a href="TipoPersona" class="btn s3"> Tipo Personas </a>
        <a href="TipoDocumentos" class="btn s3"> Tipo Documentos </a>
        <a href="TipoMaterial" class="btn s3"> Tipo Material </a>
        <a href="TipoPrestamo" class="btn s3"> Tipo Prestamo </a> 
        <a href="TipoSancion" class="btn s3"> Tipo Sancion </a> 
       </div>
    </div>
    
</div>

  
<?php
$this->load->view('footer_tpl');
?>