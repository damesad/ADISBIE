<?php
$this->load->view('header_user_tpl');
?>

<div class="card-panel yellow">
	
    <div class="row" style="margin:0 auto;width:90%;">
       <div>
          <h4 class="s10">Como Bibliotecario Puedes : </h4>
       </div>
       <div class="divider black "></div>
       <div class="section">
             <a href="Devoluciones" class="btn s4"> Registrar Devoluciones </a>
              <a href="Sancionar" class="btn s4"> Sancionar Usuario </a>
       </div>
       <div class="divider red "></div>
       <div class="section">
         <p>Administrar Materiales.</p>
        <a href="Materiales" class="btn s3"> Materiales</a>
        <a href="Editoriales" class="btn s3"> Editoriales </a>
        <a href="Autores" class="btn s3"> Autores </a>
        <a href="AutorMateriales" class="btn s3"> Autor del Material </a> 
       </div>
    </div>
    
</div>
  
<?php
$this->load->view('footer_tpl');
?>